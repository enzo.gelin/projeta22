<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\CourseRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserRepository;

class UserGetAllController
{
    public function __invoke(UserRepository $userRepository,ServiceRepository $serviceRepository,CourseRepository $courseRepository, int $id)
    {
        $data = [];
        $user = $userRepository->findOneBy(["id"=>$id]);

        $data["user"] = $user;
        $data["services"] = $serviceRepository->findUserServices($user);
        $data["courses"] = $courseRepository->findUserCourses($user);
        return $data;
    }

}
