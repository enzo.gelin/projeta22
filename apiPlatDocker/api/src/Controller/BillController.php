<?php
// api/src/Controller/BillController.php

namespace App\Controller;

use App\Entity\Bill;
use App\Entity\Service;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;


class BillController extends AbstractController
{
    public function createPdf(ManagerRegistry $doctrine, int $id)
    {
        // Recuperer la ligne Bill liée a l'id dans la base de données /srv/api/public
        $bill = $doctrine->getRepository(Bill::class)->find($id);
        if (!$bill) {
            // throw $this->createNotFoundException(
            //     'No bill found for id '.$id
            // );
        }

        // Recuperer la ligne User (vendeur) avec info contenu dans Bill
        // $vendeur = $doctrine->getRepository(User::class)->find($bill->getIdVendeur());

        // Recuperer la ligne User (acheteur) avec info contenu dans Bill
        // $acheteur = $doctrine->getRepository(User::class)->find($bill->getIdAcheteur());


        // Recuperer la ligne Services avec info contenu dans Bill
        // $service = $doctrine->getRepository(Service::class)->find($bill->getIdService());

        // Executer la commande python

        // python python_pdf/run.py $path  $date_now $id_commande $vendeur_nom $vendeur_prenom $service_intitulé $service_montantheure $service_nombreH
        $path = "/srv/api/public/testscript/devistest.pdf";
        $date_now = "2021-02-21";
        $id_commande = "2";
        $vendeur_nom = "bol";
        $vendeur_prenom = "der";
        $service_name = "dedede";
        $service_number_h = "3";
        $service_price = "45";
        $acheteur_nom = "jac";
        $acheteur_prenom = "tol";

        // Commande builde
        $string_command_line_python = "python /srv/api/public/testscript/run.py {$path} {$date_now} {$id_commande} {$vendeur_nom} {$vendeur_prenom} {$service_name} {$service_number_h} {$service_price} {$acheteur_nom} {$acheteur_prenom}";
        $process = new Process(['python', '/srv/api/public/testscript/run.py', $path, $date_now, $id_commande, $vendeur_nom, $vendeur_prenom, $service_name, $service_number_h, $service_price, $acheteur_nom, $acheteur_prenom]);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        echo $process->getOutput();

        return new Response(
            $string_command_line_python,
            Response::HTTP_OK
        );
    }
}
