<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BillRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(itemOperations={
 *     "get",
 *     "post_publication"={
 *         "method"="POST",
 *         "path"="/books/{id}/publication",
 *         "controller"=CreateBookPublication::class,
 *     }
 * })
 * @ORM\Entity(repositoryClass=BillRepository::class)
 */
class Bill
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_vendeur;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_acheteur;

     /**
     * @ORM\Column(type="integer")
     */
    private $id_services;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $start;


    /**
     * @ORM\Column(type="datetime")
     */
    private $end;


    /**
     * @ORM\Column(type="float")
     */
    private $price;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdVendeur(): ?int
    {
        return $this->id_vendeur;
    }

    public function setIdVendeur(int $id_vendeur): self
    {
        $this->id_vendeur = $id_vendeur;

        return $this;
    }

    public function getIdAcheteur(): ?int
    {
        return $this->id_acheteur;
    }

    public function setIdAcheteur(int $id_acheteur): self
    {
        $this->id_acheteur = $id_acheteur;

        return $this;
    }

    public function getIdService(): ?int
    {
        return $this->id_services;
    }

    public function setIdService(int $id_services): self
    {
        $this->id_services = $id_services;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
}
