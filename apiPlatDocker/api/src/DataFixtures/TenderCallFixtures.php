<?php

namespace App\DataFixtures;

use App\Entity\TenderCall;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TenderCallFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $users = $manager->getRepository(User::class)->findAll();

        for ($i = 0; $i < 15; $i++) {
            $tenderCall = new TenderCall();
            $tenderCall->setTitle($faker->unique()->sentence());
            $tenderCall->setStartDate($faker->dateTime());
            $tenderCall->setEndDate($faker->dateTime());
            $tenderCall->setDescription($faker->text(1000));
            $tenderCall->setCaller($users[array_rand($users)]);
            $manager->persist($tenderCall);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}