<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Service;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ServiceFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $categories = $manager->getRepository(Category::class)->findAll();
        $users = $manager->getRepository(User::class)->findAll();

        for ($i = 0; $i < 50; $i++) {
            $service = new Service();
            $service->setName('Service '.$i);
            $service->addCategory($categories[array_rand($categories)]);
            $service->setPrice($faker->numberBetween(5,500));
            $service->setDescription($faker->sentence);
            $service->setProvider($users[array_rand($users)]);
            $manager->persist($service);
        }

    $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CategoryFixtures::class,
        ];
    }
}