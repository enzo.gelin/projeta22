<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class UserFixtures extends Fixture
{
    // pwd = root
    private const PWD = '$argon2id$v=19$m=65536,t=4,p=1$r5Zd+DR1rBJy4WSB0P/1fg$Ab3xgKnmE+Zsk55QPIjljWFuLj55iG9ItdvTLaGFwGI';

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $userAdmin = (new User())
            ->setEmail('root@root.fr')
            ->setUsername('root')
            ->setPassword(self::PWD)
            ->setFirstname('root')
            ->setLastname('root')
            ->setLocalisation('Paris')
            ->setStatus(1)
            ->setIsVerified(true)
            ->setRoles(['ROLE_ADMIN']);

        $manager->persist($userAdmin);



        $user = (new User())
            ->setEmail('user@root.fr')
            ->setUsername('user')
            ->setPassword(self::PWD)
            ->setFirstname('user')
            ->setLastname('user')
            ->setLocalisation('Paris')
            ->setStatus(1)
            ->setIsVerified(true)
            ->setRoles(['ROLE_USER']);

        $manager->persist($user);

        for ($i = 0; $i < 10; ++$i) {
            $user = (new User())
                ->setEmail($faker->email)
                ->setUsername($faker->userName)
                ->setPassword(self::PWD)
                ->setFirstname($faker->firstName)
                ->setLastname($faker->lastName)
                ->setLocalisation($faker->city)
                ->setStatus(1)
                ->setIsVerified(true);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
