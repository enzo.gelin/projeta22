<?php

namespace App\DataFixtures;

use App\Entity\Review;
use App\Entity\Service;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ReviewFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $services = $manager->getRepository(Service::class)->findAll();
        $users = $manager->getRepository(User::class)->findAll();

        for ($i = 0; $i < 100; ++$i) {
            $review = (new Review())
                ->setContent($faker->sentence)
                ->setRating(random_int(1, 3))
                ->setStatus(random_int(1, 2))
                ->setReviewer($users[array_rand($users)])
                ->setService($services[array_rand($services)]);

            $manager->persist($review);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            ServiceFixtures::class,
        ];
    }
}
