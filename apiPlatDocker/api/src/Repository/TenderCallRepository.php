<?php

namespace App\Repository;

use App\Entity\TenderCall;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TenderCall|null find($id, $lockMode = null, $lockVersion = null)
 * @method TenderCall|null findOneBy(array $criteria, array $orderBy = null)
 * @method TenderCall[]    findAll()
 * @method TenderCall[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TenderCallRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenderCall::class);
    }

    // /**
    //  * @return TenderCall[] Returns an array of TenderCall objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TenderCall
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
