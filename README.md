## Commandes principales 

* Lancer le projet :
 ```docker-compose up -d```

* Supprimer le schéma de base de données : 

``` docker-compose exec php bin/console d:s:d --force```

* Creer / mise à jour du schéma de base de données : 

``` docker-compose exec php bin/console d:s:u --force ```

* Charger les fixtures dans la base de données :

``` docker-compose exec php bin/console doctrine:fixtures:load ```

* Lancer la partie vue :
``` npm run-script serve ```