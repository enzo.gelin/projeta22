# canvas_form.py
from this import d
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
import sys



# path,date,id_command,coach_data,offre
def form():
    # lecture des params de la commande
    path = str(sys.argv[1])
    date = str(sys.argv[2])
    id_command = str(sys.argv[3])
    coach_data_name = str(sys.argv[4])
    coach_data_lastname = str(sys.argv[5])
    offre_int = str(sys.argv[6])
    offre_nb_unit = strsys.argv[7])
    offre_prix = str(sys.argv[8])
    acheteur_nom = str(sys.argv[9])
    acheteur_prenom = str(sys.argv[10])
    
    acheteur_date = [acheteur_prenom,acheteur_nom]
    coach_data = [coach_data_name,coach_data_lastname]
    offre = [offre_int,offre_nb_unit,offre_prix]
    id_end = 'Devis #'+str(id_command)
    
    montant = float(offre[1]) * float(offre[2])

    # Creation du pdf
    my_canvas = canvas.Canvas(path, pagesize=letter)
    my_canvas.setLineWidth(.3)
    my_canvas.setFont('Helvetica', 12)
    my_canvas.drawString(30, 750, id_end)
    my_canvas.drawString(30, 735, 'Presta Work')
    my_canvas.drawString(500, 750, date)
    my_canvas.line(480, 747, 580, 747)

    my_canvas.drawString(30, 703, 'Offre proposée par:')
    my_canvas.line(135, 700, 580, 700)
    my_canvas.drawString(135, 703, str(coach_data[0])+ ' ' + str(coach_data[1]))

    my_canvas.drawString(30, 600, 'Offre :')
    my_canvas.drawString(30, 570, str(offre[0]))
    my_canvas.drawString(150, 570, str(offre[2]) + ' unité(s)')
    my_canvas.drawString(400, 570, str(offre[1]) + ' €/heure')


    my_canvas.drawString(30, 550, 'Montant TTC test:')
    my_canvas.drawString(475, 550, str(montant)+' €')
    my_canvas.line(30, 547, 580, 547)

    my_canvas.save()

if __name__ == '__main__':
    # parametres
    # 1 - nom du fichier qu'on veut a l'enregistrement
    # 2 - date du jour
    # 3 - numero de commande
    # 4 - vendeur info array [ nom , prenom]
    # 5 - offre prix array [ intitulé , montant par heure, nombre d'heure]
    # form('devis.pdf','02-10-2021',230,['John','Doe'],['Ménage intégral',45.2,5])
    form()