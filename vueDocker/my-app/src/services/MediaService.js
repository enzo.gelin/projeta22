export async function getAllMedias() {
    const response = await fetch('https://localhost:8443/media?page=1');
    return await response.json();
}

export async function createMedia(data) {
    const response = await fetch(`https://localhost:8443/media`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({link: data})
    })
    return await response.json();
}