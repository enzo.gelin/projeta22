import axios from 'axios';
var token = sessionStorage.getItem("token");

export default axios.create({
    baseURL: `https://localhost:8443/`,
    headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token //the token is a variable which holds the token
    },
});
