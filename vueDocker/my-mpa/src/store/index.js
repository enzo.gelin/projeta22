import { createStore } from 'vuex';

import coachesModule from './modules/coaches/index.js';
import categoriesModule from './modules/categories/index.js';
import requestsModule from './modules/requests/index.js';
import servicesModule from './modules/services/index.js';
import loginModule from './modules/login/index.js';
import usersModule from './modules/users/index.js';
import tenderCallsModule from './modules/tenderCalls/index.js';

const store = createStore({
  modules: {
    coaches: coachesModule,
    categories: categoriesModule,
    requests: requestsModule,
    services: servicesModule,
    login: loginModule,
    users: usersModule,
    tenderCalls: tenderCallsModule
  },
  state() {
    return {
      userId: 'c3'
    };
  },
  getters: {
    userId(state) {
      return state.userId;
    }
  }
});

export default store;