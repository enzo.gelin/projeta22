import axios from 'axios'

export default {
  async registerService(context, data) {
    const loginData = {
       email: data.email,
       password: data.password,
    };
    
    await axios
        .post('https://localhost:8443/authentication_token', loginData)
        .then(response => {
          console.log(response)
          sessionStorage.setItem('token', response.data.token)
          window.location = '/';
        }).catch(error => {
            console.log("Error login")
      })
  },

  async loadServices(context, payload) {

    if (!payload.forceRefresh && !context.getters.shouldUpdate) {
      return;
    }

    const response = await fetch(
      `https://localhost:8443/authentication_token`
    );
    const responseData = await response.json();

    if (!response.ok) {
      const error = new Error(responseData.message || 'Failed to fetch!');
      throw error;
    }

    const services = [];
    
    for (const key in responseData["hydra:member"]) {
      const service = {
        email: responseData["hydra:member"][key].email,
        password: responseData["hydra:member"][key].password,
        
      };
      services.push(service);
    }
    
    context.commit('setServices', services);
    context.commit('setFetchTimestamp');
  }
};
