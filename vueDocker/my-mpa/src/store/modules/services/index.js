import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';

export default {
  namespaced: true,
  state() {
    return {
      lastFetch: null,
      services: [
        // {
        //   id: 'c1',
        //   name: 'Service 1',
        //   category: ['home', 'out', 'career'],
        //   description:
        //     "Service1",
        //   price: 10
        // },
        // {
        //   id: 'c2',
        //   name: 'Service 2',
        //   category: ['home', 'career'],
        //   description:
        //     'Service2',
        //   price: 30
        // }
      ]
    };
  },
  mutations,
  actions,
  getters
};
