export default {
  registerService(state, payload) {
    state.services.push(payload);
  },
  setServices(state, payload) {
    state.services = payload;
  },
  setFetchTimestamp(state) {
    state.lastFetch = new Date().getTime();
  }
};