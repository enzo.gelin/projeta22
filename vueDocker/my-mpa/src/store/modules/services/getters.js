export default {
  services(state) {
    return state.services;
  },
  hasServices(state) {
    return state.services && state.services.length > 0;
  },
  isService(_, getters, _2, rootGetters) {
    const services = getters.services;
    const userId = rootGetters.userId;
    return services.some(service => service.id === userId);
  },
  shouldUpdate(state) {
    const lastFetch = state.lastFetch;
    if (!lastFetch) {
      return true;
    }
    const currentTimeStamp = new Date().getTime();
    return (currentTimeStamp - lastFetch) / 1000 > 60;
  }
};