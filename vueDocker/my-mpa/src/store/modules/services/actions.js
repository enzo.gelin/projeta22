import axios from 'axios'

export default {
  async registerService(context, data) {
    const userId = context.rootGetters.userId;
    const serviceData = {
      name: data.name,
      description: data.desc,
      price: data.price,
      category: data.category
    };

    const response = await fetch(
      `https://vue-http-demo-85e9e.firebaseio.com/services/${userId}.json`,
      {
        method: 'PUT',
        body: JSON.stringify(serviceData)
      }
    );

    // const responseData = await response.json();

    if (!response.ok) {
      // error ...
    }

    context.commit('registerService', {
      ...serviceData,
      id: userId
    });
  },
  async loadServices(context, data) {
    var token = sessionStorage.getItem("token"); 
    await axios
      .get('https://localhost:8443/services?page=1', {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token //the token is a variable which holds the token
        }
      })
      .then(response => {
        console.log('Success ')
        const responseData = response.data;
       
        const services = [];
    
        for (const key in responseData["hydra:member"]) {
          const service = {
            id: key,
            name: responseData["hydra:member"][key].name,
            description: responseData["hydra:member"][key].description,
            price: responseData["hydra:member"][key].price,
            category: [responseData["hydra:member"][key].categories],
            provider: responseData["hydra:member"][key].provider,
    
          };
          services.push(service);
        }
        console.log(services)
        context.commit('setServices', services);
        context.commit('setFetchTimestamp');

      });


  },
  async loadServicesOld(context, payload) {

    if (!payload.forceRefresh && !context.getters.shouldUpdate) {
      return;
    }

    const response = await fetch(
      `https://localhost:8443/services?page=1`
    );
    const responseData = await response.json();

    if (!response.ok) {
      const error = new Error(responseData.message || 'Failed to fetch!');
      throw error;
    }

    const services = [];

    for (const key in responseData["hydra:member"]) {
      const service = {
        id: key,
        name: responseData["hydra:member"][key].name,
        description: responseData["hydra:member"][key].description,
        price: responseData["hydra:member"][key].price,
        category: [responseData["hydra:member"][key].categories],
        provider: responseData["hydra:member"][key].provider,

      };
      services.push(service);
    }

    context.commit('setServices', services);
    context.commit('setFetchTimestamp');
  }
};
