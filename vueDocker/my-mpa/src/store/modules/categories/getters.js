export default {
  categories(state) {
    return state.categories;
  },
  hasCategories(state) {
    return state.categories && state.categories.length > 0;
  },
  isCategorie(_, getters, _2, rootGetters) {
    const categories = getters.categories;
    const userId = rootGetters.userId;
    return categories.some(categorie => categorie.id === userId);
  },
  shouldUpdate(state) {
    const lastFetch = state.lastFetch;
    if (!lastFetch) {
      return true;
    }
    const currentTimeStamp = new Date().getTime();
    return (currentTimeStamp - lastFetch) / 1000 > 60;
  }
};