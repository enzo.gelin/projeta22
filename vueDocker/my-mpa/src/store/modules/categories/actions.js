import axios from 'axios'

export default {
  async registerCategorie(context, data) {
    console.log('registerCategorie')
    const formData = {
      name: data.name,
    };
    var token = sessionStorage.getItem("token");
    await axios
      .post('https://localhost:8443/categories', formData,{
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token //the token is a variable which holds the token
        }
      })
      .then(response => {
        console.log("Response")
        console.log(response)
      }).catch(error => {
        console.log("Error")
        console.log(error)
      })

      
  },
  async loadCategories(context, data) {
    var token = sessionStorage.getItem("token");
    await axios
      .get('https://localhost:8443/categories?page=1', {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token //the token is a variable which holds the token
        }
      })
      .then(response => {
        console.log('Success ')
        const responseData = response.data;

        const categories = [];

        for (const key in responseData["hydra:member"]) {
          const categorie = {
            id: responseData["hydra:member"][key].id,
            name: responseData["hydra:member"][key].name,

          };
          categories.push(categorie);
        }
        console.log(categories)
        context.commit('setCategories', categories);
        context.commit('setFetchTimestamp');

      }).catch(error => {
        console.log("Error:", error)
      })


  },
  async updateCategorie(context, data) {

    console.log('updateCategorie')
    const formData = {
      name: data.name,
    };
    var token = sessionStorage.getItem("token");
    await axios
      .put('https://localhost:8443/categories/'+ data.id, formData,{
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token //the token is a variable which holds the token
        }
      })
      .then(response => {
        console.log("Response")
        console.log(response)
      }).catch(error => {
        console.log("Error")
        console.log(error)
      })

  },
  async deleteCategorie(context, data) {

    console.log('deleteCategorie')
    console.log(data.id)
    var token = sessionStorage.getItem("token");
    await axios
      .delete('https://localhost:8443/categories/'+ data.id , {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token //the token is a variable which holds the token
        }
      })
      .then(response => {
        console.log("Response")
        console.log(response)
      }).catch(error => {
        console.log("Error")
        console.log(error)
      })

  }
};