import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';

export default {
  namespaced: true,
  state() {
    return {
      lastFetch: null,
      categories: [
        {
          id: 'c1',
          name: 'Categorie 1',
          category: ['home', 'out', 'career'],
          description:
            "Categorie1",
          price: 10
        },
        {
          id: 'c2',
          name: 'Categorie 2',
          category: ['home', 'career'],
          description:
            'Categorie2',
          price: 30
        }
      ]
    };
  },
  mutations,
  actions,
  getters
};
