export default {
    registerUser(state, payload) {
        state.users.push(payload);
    },
    setUsers(state, payload) {
        state.users = payload;
    },
    setSessionUser(state, payload) {
        state.sessionUser = payload;
    },
    setUser(state, payload) {
        state.user = payload;
    },
    setFetchTimestamp(state) {
        state.lastFetch = new Date().getTime();
    }
};

