export default {
    users(state) {
        return state.users;
    },
    user(state) {
        return state.user;
    },
    sessionUser(state) {
        return state.sessionUser;
    },
    hasUsers(state) {
        return state.users && state.users.length > 0;
    },
    isUser(_, getters, _2, rootGetters) {
        const users = getters.users;
        const userId = rootGetters.userId;
        return users.some(user => user.id === userId);
    },
    shouldUpdate(state) {
        const lastFetch = state.lastFetch;
        if (!lastFetch) {
            return true;
        }
        const currentTimeStamp = new Date().getTime();
        return (currentTimeStamp - lastFetch) / 1000 > 60;
    }
};