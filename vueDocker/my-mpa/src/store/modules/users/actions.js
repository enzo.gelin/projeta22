import API from '../../../api'
import parseToken from '../../../pages/account/parseToken'

export default {
    async registerUser(context, data) {
        console.log('registerUser')
        const formData = {
            firstname: data.firstname,
            lastname: data.lastname,
            email: data.email,
            username: data.username,
            localisation: data.localisation,
            roles: data.roles,
            password: data.password,
            status: data.status,
            isVerified: data.isVerified
        };
        await API
            .post('users', formData)
            .then(response => {
                console.log("Response")
                console.log(response)
            }).catch(error => {
                console.log("Error")
                console.log(error)
            })
    },
    async loadUsers(context, data) {
        await API
            .get('users?page=1')
            .then(response => {
                console.log('Success ')
                const responseData = response.data;

                let users = [];

                for (const key in responseData["hydra:member"]) {
                    const user = {
                        id: responseData["hydra:member"][key].id,
                        firstname: responseData["hydra:member"][key].firstname,
                        lastname: responseData["hydra:member"][key].lastname,
                        email: responseData["hydra:member"][key].email,
                        username: responseData["hydra:member"][key].username,
                        localisation: responseData["hydra:member"][key].localisation,
                        roles: responseData["hydra:member"][key].roles,
                        status: responseData["hydra:member"][key].status,
                    };
                    users.push(user);
                }
                console.log(users)
                context.commit('setUsers', users);
                context.commit('setFetchTimestamp');

            }).catch(error => {
                console.log("Error:", error)
            })
    },
    async updateUser(context, data) {
        console.log("i'm called")
        const formData = {
            firstname: data.firstname,
            lastname: data.lastname,
            email: data.email,
            username: data.username,
            localisation: data.localisation,
            roles: data.roles,
            password: data.password,
            status: data.status,
            isVerified: data.isVerified
        };
        await API.put('users/'+ data.id, formData)
            .then(response => {
                console.log("Response")
                console.log(response)
            }).catch(error => {
                console.log("Error")
                console.log(error)
            })

    },
    async deleteUser(context, data) {
        await API
            .delete('users/'+ data.id)
            .then(response => {
                console.log("Response")
                console.log(response)
            }).catch(error => {
                console.log("Error")
                console.log(error)
            })

    },
    async userProfiler(context, data){
        let responseData = [];
        let renderdata = {};
        let userCourses = [];
        await API.get('users/get/'+ data.id+'/getall/')
            .then(response => {
                responseData = response.data;  
                renderdata = {
                    user: responseData["hydra:member"][0],
                    services: responseData["hydra:member"][1],
                    courses:responseData["hydra:member"][2],
                }
                context.commit('setUser', renderdata);
                context.commit('setFetchTimestamp'); 
            })
            .catch(error => {
                console.log("Error:", error)
            })

        /**/
           
    },
    async sessionUser(context, data){
        let responseData = [];
        let renderdata = {};
        let userCourses = [];
        let token = parseToken.parseToken
        await API.get('users?email='+ token().username)
            .then(response => {
                responseData = response.data;  
                console.log("success sessionUser")
                context.commit('setSessionUser', responseData["hydra:member"][0]);
                context.commit('setFetchTimestamp'); 
            }).catch(error => {
                console.log("Error:", error)
            })

        /**/
           
    }
};
