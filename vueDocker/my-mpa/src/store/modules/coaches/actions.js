import axios from "axios";

export default {
  async registerCoach(context, data) {
    console.log('registerCoach')
    const formData = {
      firstname: data.firstname,
      lastname: data.lastname,
      email: data.email,
      username: data.username,
      localisation: data.localisation,
      createdAt: "2022-01-18T13:30:14+00:00",
      roles: ["ROLE_ADMIN", "ROLE_USER"],
      password: "password",
      status: 1,
      isVerified: true,
      reviews: ["/reviews/5"],
      services: ["/services/13"]

      //ajouter ls champsfirstname + mettre le bon lien

    };
    var token = sessionStorage.getItem("token");
    await axios
      .post('https://localhost:8443/users', formData, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token //the token is a variable which holds the token
        }
      })
      .then(response => {
        console.log("Response")
        console.log(response)
      }).catch(error => {
        console.log("Error")
        console.log(error)
      })

    // context.commit('registerCoach', {
    //   ...coachData,
    //   id: userId
    // });
  },

  async loadCoaches(context, payload) {
    var token = sessionStorage.getItem("token");
    // if (!payload.forceRefresh && !context.getters.shouldUpdate) {
    //   return;
    // }

    await axios
      .get('https://localhost:8443/users?page=1', {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token //the token is a variable which holds the token
        }
      })
      .then(response => {
        console.log('Success ')
        const responseData = response.data;

        const coaches = [];

        for (const key in responseData["hydra:member"]) {
          const coach = {
            id: key,
            firstname: responseData["hydra:member"][key].firstname,
            lastname: responseData["hydra:member"][key].lastname,
            email: responseData["hydra:member"][key].email,
            username: responseData["hydra:member"][key].username,
            localisation: responseData["hydra:member"][key].localisation,
            createdAt: responseData["hydra:member"][key].createdAt,
          };
          coaches.push(coach);
        }
        console.log(coaches)
        context.commit('setCoaches', coaches);
        context.commit('setFetchTimestamp');
      });
  }
}