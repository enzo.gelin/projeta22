export default {
    registerTenderCall(state, payload) {
        state.tenderCalls.push(payload);
    },
    setTenderCalls(state, payload) {
        state.tenderCalls = payload;
    },
    setTenderCall(state, payload) {
        state.tendercall = payload;
    },
    setFetchTimestamp(state) {
        state.lastFetch = new Date().getTime();
    }
};

