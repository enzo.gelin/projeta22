export default {
    tenderCalls(state) {
        return state.tenderCalls;
    },
    tenderCall(state) {
        return state.tenderCalls;
    },
    hasTenderCalls(state) {
        return state.tenderCalls && state.tenderCalls.length > 0;
    },
    isTenderCall(_, getters, _2, rootGetters) {
        const tenderCalls = getters.tenderCalls;
        const tenderCallId = rootGetters.userId;
        return tenderCalls.some(tenderCall => tenderCall.id === tenderCallId);
    },
    shouldUpdate(state) {
        const lastFetch = state.lastFetch;
        if (!lastFetch) {
            return true;
        }
        const currentTimeStamp = new Date().getTime();
        return (currentTimeStamp - lastFetch) / 1000 > 60;
    }
};