import API from '../../../api'
import parseToken from '../../../pages/account/parseToken'

export default {
    async registerTenderCall(context, data) {

        console.log('registertTnderCall')
        const formData = {
            title: data.title,
            description: data.description,
        };
        await API
            .post('/tender_calls', formData)
            .then(response => {
                console.log("Response")
                console.log(response)
            }).catch(error => {
                console.log("Error")
                console.log(error)
            })
    },
    async loadTenderCalls(context, data) {
        await API
            .get('/tender_calls?page=1')
            .then(response => {
                console.log('Success ')
                const responseData = response.data;

                let tenderCalls = [];

                for (const key in responseData["hydra:member"]) {
                    const tenderCall = {
                        id: responseData["hydra:member"][key].id,
                        title: responseData["hydra:member"][key].title,
                        startDate: responseData["hydra:member"][key].startDate,
                        endDate: responseData["hydra:member"][key].endDate,
                        description: responseData["hydra:member"][key].description,
                        caller: responseData["hydra:member"][key].description
                    };
                    tenderCalls.push(tenderCall);
                }
                console.log(tenderCalls)
                context.commit('setTenderCalls', tenderCalls);
                context.commit('setFetchTimestamp');

            }).catch(error => {
                console.log("Error:", error)
            })
    },
    async updateTenderCall(context, data) {
        console.log("i'm called")
        const formData = {
            title: data.title,
            startDate: data.startDate,
            endDate: data.endDate,
            description: data.description,
        };
        await API.put('tender-calls/'+ data.id, formData)
            .then(response => {
                console.log("Response")
                console.log(response)
            }).catch(error => {
                console.log("Error")
                console.log(error)
            })

    },
    async deleteTenderCall(context, data) {
        await API
            .delete('tender-calls/'+ data.id)
            .then(response => {
                console.log("Response")
                console.log(response)
            }).catch(error => {
                console.log("Error")
                console.log(error)
            })

    },
};
