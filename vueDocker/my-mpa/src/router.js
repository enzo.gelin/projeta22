import { createRouter, createWebHistory } from 'vue-router';

import CoachDetail from './pages/coaches/CoachDetail.vue';
import CoachRegistration from './pages/coaches/CoachRegistration.vue';
import CoachesList from './pages/coaches/CoachesList.vue';
import ServiceDetail from './pages/services/ServiceDetail.vue';
import ServicesList from './pages/services/ServicesList.vue';
import ContactCoach from './pages/requests/ContactCoach.vue';
import ContactService from './pages/requests/ContactService.vue';
import RequestsReceived from './pages/requests/RequestsReceived.vue';
import NotFound from './pages/NotFound.vue';
import HomePage from './pages/HomePage.vue';
import ServiceRegistration from "./pages/services/ServiceRegistration";
import CategoriesList from "./pages/category/CategoriesList";
import Login from './pages/authentification/LoginPage.vue'
import CategoryEdit from "./pages/category/CategoryEdit";
import CategoryAdd from "./pages/category/CategoryAdd";
import UsersList from "./pages/user/UsersList";
import UserEdit from "./pages/user/UserEdit";
import UserAdd from "./pages/user/UserAdd";
import Inscription from "./pages/authentification/InscriptionPage";
import UserProfile from "./pages/user/UserProfile";
import AccountIndex from "./pages/account/AccountIndex";
import TenderCallsList from "./pages/tenderCalls/TenderCallsList";
import TenderCallAdd from "./pages/tenderCalls/TenderCallAdd";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', redirect: '/accueil' },
    { path: '/accueil', component: HomePage },
    { path: '/mon-compte', component: AccountIndex,meta: { requiresAuth: true } },
    { path: '/services', component: ServicesList,meta: { requiresAuth: true } },
    { path: '/coaches', component: CoachesList,meta: { requiresAuth: true } },
    {
      path: '/coaches/:id',
      component: CoachDetail,
      meta: { requiresAuth: true },
      children: [
        { path: 'contact', component: ContactCoach,meta: { requiresAuth: true } } // /coaches/c1/contact
      ]
    },
    {
      path: '/services/:id',
      component: ServiceDetail,
      meta: { requiresAuth: true },
      children: [
        { path: 'contact/:id_coach', component: ContactService,meta: { requiresAuth: true } } // /services/c1/contact
      ]
    },
    { path: '/services/ajouter', component: ServiceRegistration,meta: { requiresAuth: true } },
    { path: '/requests', component: RequestsReceived },
    { path: '/administration/categories', component: CategoriesList, name:"categoryList",meta: { requiresAdmin: true } },
    { path: '/administration/categories/:id', component: CategoryEdit , name:"categoryEdit",meta: { requiresAdmin: true }},
    { path: '/administration/categories/ajouter', component: CategoryAdd , name:"categoryAdd",meta: { requiresAdmin: true }},
    { path: '/administration/utilisateurs', component: UsersList, name:"userList",meta: { requiresAdmin: true } },
    { path: '/administration/utilisateurs/ajouter', component: UserAdd, name:"userAdd",meta: { requiresAdmin: true } },
    { path: '/administration/utilisateurs/modifier/:id', component: UserEdit, name:"userEdit",meta: { requiresAdmin: true } },
    { path: '/utilisateurs/profile/:id', component: UserProfile, name:"userProfile" },
    { path: '/appels-offre', component: TenderCallsList, name:"tenderCallsList"},
    { path: '/appels-offre/ajouter', component: TenderCallAdd, name:"tenderCallAdd"},
    { path: '/login', component: Login },
    { path: '/connexion', component: Inscription},
    { path: '/register', component: CoachRegistration },
    { path: '/:notFound(.*)', component: NotFound }
  ]
});

router.beforeEach((to, from) => {
  var token = sessionStorage.getItem("token");
  var statut_role =  ['ROLE_USER'];
  if(token){
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    var decodeTk = JSON.parse(jsonPayload)
    var statut_role = decodeTk.roles;
  }

  if (to.meta.requiresAuth && !token) {
    return {
      path: '/login',
      query: { redirect: to.fullPath },
    }
  } else if(to.meta.requiresAdmin && !statut_role.includes('ROLE_ADMIN')) {
    return {
      path: '/'
    }
  }
})

export default router;
